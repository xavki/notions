%title: Notions
%author: xavki


# CI / CD


<br>


* Continuous integration


* Continuous delivery


* Continuous deployment


-------------------------------------

# Continuous Integration


<br>


* Objectifs :
	* rammener les développements (feature) dans la branche principale (dev)
	* construire l'applicatif (build)
	* réaliser les tests unitaires (applicatifs)
	* prise en compte et capitalisation sur les bugs précédents pour réaliser les tests
	* éviter les problèmes d'intégration : casser l'applicatif au merge
	* déclenchement sur le push des développeurs
	* l'équipe de tests perd moins de temps si de bons tests unitaires
	* corrections rapides par les devs suite au push

<br>


DEV >> BUILD >> TESTS U


---------------------------------------


# Continuous Delivery


<br>


* Objectifs :
	* délivrer l'application
	* releaser l'application (y apposer une version)
	* tests approfondis (outils automatiques, tests manuels)
	* fréquence de release
	* délai court avec le continuous deploiement = efficacité (éviter décalage)
	* attention aux tests et en leur confiance
	* préparation importante pour l'automatisation de l'étape suivante (confiance)
	* capitalisation sur les runs précédents
	* relativiser la mise en production (itération)
	* retour rapide si bug
	
<br>


DEV >> BUILD >> TESTS U >> TESTS ALL >> RELEASE

-----------------------------------------


# Continuous Deployment



<br>


* Objectifs :
	* mise en production automatique sans intervention
	* installer l'applicatif
	* gérer ses dépendances (bases de données, monitoring...)
	* responsabiliser les devs en raccourcissant leur mise en production (satisfaction)
	* automatisation = processus >> prise en compte organisation générale (communication)
	* fail fast

<br>


DEV >> BUILD >> TESTS U >> TESTS ALL >> RELEASE >> DEPLOY


-------------------------------------------


# Quelques outils


<br>


Jenkins : déploiement - totalement gratuit

<br>


Gitlab : déploiement et dépots - gratuit puis payant

<br>


CircleCI : déploiement et tests - gratuit puis payant

<br>


Jmeter : tests - gratuit

<br>


Postman : tests (API) - gratuit puis payant

<br>


Ansible : déploiement - gratuit

<br>


SonarQube : analyse de code - gratuit puis payant


