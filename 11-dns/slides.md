%title: Notions
%author: xavki


# DNS : principe


<br>


* date de 1983 - avant fonctionnement par fichiers HOSTS


<br>


* DNS = Domain Name Services


<br>


* machines communiquent par IP


<br>


* résolution DNS > faire correspondre des noms et les IP
	* nom = FQDN ou Fully Qualified Domain Name


<br>


* fonctionnement par annuaire > dns
	ex : wiki.xavki.blog

-----------------------------------------------------------------------------------------

# DNS : Fonctionnement hiérarchique


<br>


* "." : racine
	* 13 serveurs racine (<a-m>.root-server.net)
	* organisés par ICANN (Internet Corporation for Assigned Names and Numbers)
	* chaque serveur dispose de redondance et répartition


<br>


* "blog" : TLD ou Top Level Domain
	* répartis et férés par des organismes
	* gTLD : si autre qu'un pays
	* ccTLD : si un pays (country code)


<br>


* "xavki" "wiki" : sous-domaines
	* lecture droite à gauche par délégation


<br>


* serveurs récursifs : 
	* serveurs dns comme google (8.8.8.8) ou cloudflare (1.1.1.1)
	* fournisseurs d'accès

-------------------------------------------------------------------------------------------


# DNS : Déroulement d'une résolution	


<br>


Déroulement d'une résolution : wiki.xavki.blog


<br>


1- interrogation du serveur récursif 
	(plusieurs configurés = 3) : système de cache (conservation Time To Live TTL)


<br>


2- sinon le serveur récursif se tourne vers un serveur racine = où trouver la réponse "blog" ?


<br>


3- accès au serveur en charge de "blog" : où trouver "xavki" ?




<br>


PS : Processus identique pour la résolution inverse : IP > adresse
PS2 : load balancing dns = round robin


-------------------------------------------------------------------------------------------


# DNS : Types d'enregistrement


<br>


* zones sont composés d'enregistrements (lignes ou Resource Record RR)

<br>


* A : fqdn > ipv4

<br>


* AAAA : FQDN > ipv6

<br>


* CNAME : alias ex : xavki.blog > xavki.fr

<br>


* MX : serveurs mails pour un domaine

<br>


* PTR : reverse dns

<br>


* NS : name servers

<br>


* SRV : dns avancé (load balancing)
