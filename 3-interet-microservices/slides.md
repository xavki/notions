%title: Notions - Intérêt des microservices
%author: xavki



# Cet intérêt des microservices ?

<br>


* concept utilisé dans la plupart des entreprises ou l'IT
est à la pointe : Netflix, Sarenza, Amazon, Google...


<br>


# Pourquoi ?

<br>


* périmètres restreints = plus petites équipes (pizzas)

<br>


* développement plus simple, plus rapide, plus agile

<br>


* versions plus faciles à itérer (devops)

<br>


* plus facile à tester

<br>


* base de données plus réduites moins complexes... moins sql

<br>


* meilleure gestion des défaillances : LB, circuit breaker, cache


----------------------------------------------------------------------



# Mais tout n'est pas rose


<br>


* gérer les flux entre des centaines de microservices

<br>


* sollicitation plus importante des réseaux

<br>


* infrastructure plus complexe

<br>


* création de registry de services (cf consul, zookeeper...)

<br>


* communication par API : organiser, standardiser, normer

<br>


* bonne communication entre les équipes : devs, infras, devops...

<br>


* effet de mode adapté aux technos actuelles (docker, cloud...)
