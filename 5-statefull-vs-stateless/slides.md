%title: Notions - Statefull vs stateless
%author: xavki



# Statefull vs Stateless

<br>



* une opposition d'actualité (notamment avec les instances éphémères : docker, compute...)

* s'applique aux applications

* attention il y a aussi les protocoles (SMB = statefull, NFS = stateless)

<br>



## Stateless = sans état

<br>



* ne conserve pas d'informations liées à une requête ou connexion

<br>


* des cookies peuvent permettre de stocker temporairement des données

<br>


* très à la mode car très facile à scaler horizontalement (démultiplier le nombre d'instances)

<br>


* en gros si infos stockées = côté client ou autres serveurs

ex: applications derrière un LB - stateless = LB possible même avec connexion

<br>


* plus d'appels BDD ou API

* facile à reconstruire : on détruit et on recréé

-----------------------------------------------------------------------------------------------


## Statefull = avec état


<br>



* conserve les informations

<br>


* scalabilité horizontale impossible

<br>


* ne compte pas sur le client pour stocker des infos

<br>


* plutôt pour les anciennes applis monolithiques

