%title: Notions - Déclaratif / Impératif
%author: xavki


-> Configuration déclarative vs impérative <-


* forte évolution apportée par les dernières technologies: kubernetes, docker, cloud...


<br>


Déclaratif :

* assimilable à descriptif

* exemple : docker compose, déploiements kubernetes...

* on décrit l'état désiré

* c'est le système qui s'assure de l'état désiré 

Atouts : autoréparation, maintien en conditions, simplicité de gestion

--------------------------------------------------------------------------------------------

-> Configuration déclarative vs impérative <-




<br>


Impératif:

* système dont on maîtrise l'état final

* exemple : une installation via apt

* le demandeur analyse l'état final et s'en assure

Atouts: on sait ce que l'on a et quand, gestion régulière
