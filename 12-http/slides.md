%title: Notions
%author: xavki


# HTTP : principe


<br>


* HTTP = Hypertext Transfer Protocol

* communication client / serveur

* ports par défaut : HTTP/80 et HTTPS/443

* à l'origine des échanges web avec le langage HTML
	* <1996 : 0.9
	* 1996 : 1.0
	* 1997 : 1.1
	* 2015 : 2.0

* URI ou Uniform Resource Identifier = url composée d'une requête (détermine la ressource visée)
	* ex : http://xavki.blog/v1/users/1/groupe/22

------------------------------------------------------------------------------


# HTTP et ses verbes


<br>


* GET : demander des données sur une ressource via URI
	utilisation : demander la home d'un site web

<br>


* HEAD : demande simplement des informations sur la ressource (sans données)
	utilisation : fournir date de modification, taille, type d'une ressource 

<br>


* POST : envoi de données à une ressource
	utilisation : poster un message, soumettre un formulaire

<br>


* PUT : modification d'une ressource
	utilisation : mise à jour d'une ressource, tableau...

<br>


* DELETE : suppression une ressource
	utilisation : suppression d'un message


------------------------------------------------------------------------------

# HTTP : codes d'états


<br>


* 1xx : information

<br>


* 2xx : Réussite
	* 200 : requête avec succés
	* 201 : succès avec création de document

<br>


* 3xx : redirection
	* 301 : signal de déplacement permanent (ex: migration HTTP > HTTPS)
	* 302 : déplacement temporaire
	* 310 : trop de redirections (ex: boucles de redirection)

<br>


* 4xx : erreur client
	* 400 : mauvaise requête (mal formulée)
	* 401 : connexion sans authentification alors qu'une authentification est requise
	* 403 : accès interdit (serveur apache refusant l'accès à certains répertoires)
	* 404 : ressource introuvable (ex: page web non existante) 

<br>


* 5xx : erreur serveur
	* 500 : erreur du interne du serveur (ex: application ne répond pas)
	* 502 : bad gateway (ex: application ne répond pas derrière un proxy)
	* 503 : service en maintenance

------------------------------------------------------------------------------


# HTTP : son évolution


<br>


* 0.9 : connexion client/serveur avec requête simple et un retour "vrac" sans entête

<br>


* 1.0 : apparition des entêtes
	* Host
	* Date
	* Server
	* Content-Type
	* Content-length	
	type de support 
	cache
	vérification authenticité (réduction d'accès)

<br>


* 1.1 : connexions persistantes (maintient après la première requête = gain de temps)
	multihoming : 1 serveur = plusieurs noms (importance du Host)
	vérification authenticité amélioration

<br>


* 2 : 	réduction de la latence
	compression de données des entêtes
	envoi de données par anticipation
	multiplexage de requêtes pour une même connexion
	divergence sur le chiffrement non imposé
