%title: Notions - Docker Swarm vs Kubernetes
%author: xavki



# Que font ces outils ?


<br>


* orchestrateurs de conteneurs
		* docker swarm : cf playlist
		* kubernetes : cf playlist

<br>


* mise en place et gestion de cluster docker

* pool de master (1 ou plus) et des workers (1 ou plus)

* système intégré de service discovery

* gestion de réseaux

* stockage

-------------------------------------------------------------------

# Quelques points pour comparer


<br>


* installation :
		* swarm : simple et facile avec une seule commande init
		* kubernetes : plusieurs prérequis (swap off, plusieurs packages)


<br>


* command line :
		* swarm : peu de commandes de docker complétées (facile à utiliser et retenir)
		* kubernetes : beaucoup de commandes avec beaucoup d'options (alias indispensable voir plus)


<br>


* Monitoring et logs :
		* swarm : ajout manuel, peu de solutions (à l'ancienne), Reimann
		* kubernetes : automatisation possible, ELK, prometheus et grafana


-------------------------------------------------------------------

# Quelques points pour comparer


<br>


* Scalabilité :
		* swarm : manuel
		* kubernetes : manuel ou hpa (avec des solutions plus poussées)


<br>


* rolling update :
		* swarm : peu de choix de stratégie, pas facile à utiliser (manque l'historique)
		* kubernetes : stratégie personnalisable, historique, rollback facile


<br>


* network :
		* swarm : encryption automatique, facile à configurer, peu de choix
		* kubernetes : plus de choix, plus complexe


