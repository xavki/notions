%title: Notions
%author: xavki


# Swap


<br>


* SWAP : espace d'échange

<br>


* débordement de la mémoire vive vers la mémoire de masse
	mémoire vive + mémoire de masse = mémoire virtuelle

<br>


* swapper = perdre de la performance (variable suivant les applicatifs)
	facteur 10 entre DRAM et SSD

<br>


* pas assez de mémoire vive = utilisation du swap (réservation du disque physique)

<br>


* intérêt : évite la perte de process par manque de mémoire 

<br>


* inconvénient : perfomances lentes difficiles à diagnostiquer

<br>


* solutions :
	augmenter le swap (mauvaise)
	augmenter la RAM (bonne)
	vérifier la bonne utilisation de la mémoire (bonne)

--------------------------------------------------------------------------------------------

# Commandes


<br>


* pseudo filesystem : 

```
cat /proc/swaps
cat /proc/meminfo
```

<br>


* swapon --all : affiche les devices affectés au swap

* swapon --summary 

 

<br>


* free

* top

* vmstat 

Rq :

```
dmesg | grep oom-killer
```

------------------------------------------------------------------------------------------

# Configuration


<br>


swapoff -a : désactive le swap

<br>


cat /proc/sys/vm/swappiness
sudo sysctl vm.swappiness=0 

Valeurs: 0 < 100
	* 0 : éviter le manque de mémoire
	* 1 : utilisation minimale (avant linux 3.5)
	* 10 : si système avec beaucoup de mémoire
	* 60 : valeur par défaut
	* 100 : priorité au swap
