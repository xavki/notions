%title: Notions - IAAS PAAS SAAS CAAS FAAS
%author: xavki



# IAAS PAAS SAAS CAAS FAAS

<br>



* As a Service... : à la demande (ne requiert pas d'action, d'installation)

<br>


* fortement poussé par le cloud

<br>


* tout est automatisé et automatisable

<br>


* on créé des offres en fonction du besoin client

Réseau > Stockage > Serveurs > Virtualisation > Sys expl.
Middleware > Runtime > Datas > Applications

-------------------------------------------------------------------------------------

-> Les précurseurs : IaaS PaaS SaaS <-

<br>


## IaaS

Infra as a Service

* fournisseur : Réseau + Stockage + Serveurs + Virtualisation
* utilisateur : OS + Middleware + Runtime + Datas + Applications

<br>


## PaaS

Plateforme as a Service

* fournisseur : Réseau + Stockage + Serv + Virtu + OS + Run + Middle
* utilisateur : Datas + applications

<br>


## SaaS

Software as a Service

* fournisseur : Réseau + ... + Applis
* utilisateur : utilise l'application


-----------------------------------------------------------------------

-> Les nouveaux : CaaS FaaS <-


<br>


## CaaS

Container as a Service

* fournisseur : réseau + ... + conteneurs
* utilisteur : lance des conteneurs

<br>


## FaaS

Function as a Service

* fournisseur : réseau + ... + fonctions
* client : créé/lance des fonctions



