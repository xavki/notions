%title: Notions - MESH
%author: xavki



# MESH (routing mesh)


<br>


* très tendance avec les microservices, docker et le cloud
* important pour scaling horizontal
* utile pour le circuit breaker : éviter les effets "boule de neige"
* meilleure gestion des retryables

<br>



## Pourquoi ?

Pet vs Cattle

<br>


* apprendre à ne pas savoir quel ip porte quel service ?

<br>


=> mise en place d'une registry de service : zookeeper, consul...

---------------------------------------------------------------------------

## Architecture

<br>


* les outils:

MESH = système registry + reverse-proxy sidecar + service

```
                            +-------------------------+
          +-----------------+  Registry Services      +--------------+
          |                 +-------------------------+              |
    +---------------------------------+        +--------------------------------+
    |     +                           |        |                     +          |
    |   agent            service      |        |    service      ++ agent       |
    |   registry           +          |        |    +            |  registry    |
    |      +               |          |        |    |            |              |
    |      |               +          |        |    +            |              |
    |      |                          |        |                 |              |
    |      +--+ Reverse-Proxy         |        |  Reverse-Proxy  +              |
    |                 ++              |        |        ++                      |
    +---------------------------------+        +--------------------------------+
                      |                                 |
                      +---------------------------------+
                                       ^
                                       |
                                       |
                                    +-----+
                                    |     |
                                    +-----+

```

-------------------------------------------------------------------------

## Outils

<br>


* Reverses-proxies : envoy, linkerd, nginx...

<br>


* Registry : consul, zookeeper...

<br>


* Package complet : Istio (K8S) = envoy, pilot...

<br>


Définitions :

* data plane : tenue à jour du registre

* control plane : ajout de monitoring, outils de gestion du mesh

