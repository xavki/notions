%title: Notions
%author: xavki


# HTTPS


# Pourquoi ?


<br>


* HyperText Transfer Protocol Secure


* protocole d'échange entre clients et servers
	* authentification / intégrité / confidentialité

<br>


* HTTP = échange sans chiffrement (pb Man in the middle...)

	navigateur > pirate > ecommerce

	navigateur < pirate < ecommerce

<br>


* Solution = chiffrement = HTTPS

<br>


* SSL = Secure Socket Layer

* TLS = Transport Layer Security

* Difficulté : vitesse de communication

---------------------------------------------------------------------

# Historique


<br>


1. SSL 1.0 : jamais paru (Netscape)

<br>


2. SSL 2.0 : 1995 - 2011

<br>


3. SSL 3.0 : 1996 - 2015

<br>


4. TLS 1.0 : 1999 - 2020

<br>


5. TLS 1.1 : 2006 - 2020

<br>


5. TLS 1.2 : 2008

<br>


6. TLS 1.3 : 2018

<br>


Ne pas confondre le procole TLS et le type de certificat (x509)

--------------------------------------------------------------------

# Principe de la communication

<br>


0. Serveur : obtention d'un certificat auprès d'une authorité de certification (principe clefs publique/privé)

<br>


1. Client : dit Hello au serveur et lui indique la version TLS et les suites cryptographiques supportées

<br>


2. Serveur : répond Hello au client et indique les TLS et suite crypto utilisés + certificat et clef publique

<br>


3. Client : vérifie la validité du certificat auprès de l'autorité de certification externe (différents niveaux de délégation)

<br>


4. Client : Fourni une clef symétrique pour les prochains échanges (clef chiffrée avec la clef publique fourni par le serveur)

<br>


5. Client/Serveur : échanges avec la clef symétrique

