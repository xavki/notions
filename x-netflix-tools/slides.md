%title: Notions - MESH
%author: xavki



# Netflix


* sa force : la redondance et la résilience

* perte de serveurs, de zone aws, de régions aws...
		- chaos monkey, chaos gorilla...

Service Registry with Eureka — for registration and discovery of services
Client side load balancing with Ribbon — clients can choose what server to send their requests to.
Declarative REST client Feign to talk to other services. Internally, it uses Ribbon.
API Gateway with Zuul — single entry point to manage all API calls, routing rules etc. to our
microservices
Circuit Breakers with Hystrix — handling fault tolerance along with the ability to turn off a
communication channel for a short period of time (breaking the circuit) and return a user friendly
response if the destination service is down
Dashboards with Hystrix and Turbine — visualizing traffic and circuit breakers



