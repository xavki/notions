%title: Notions - Twelve Factors
%author: xavki



# Twelve Factors

<br>


* objectif : concevoir des applications comme des services

* origine : Heroku 


<br>


# Liste

<br>


* 1- Base de code : le code à un endroit et versionné (git)

<br>


* 2- Dépendances : outil de gestion des dépendances (maven, composer, leiningen...)

<br>


* 3- Configuration : avoir un fichier de conf pour tous les environnements
avoir recours aux variables d'environnement (cf docker)

<br>


* 4- Services externes : les services doivent être gérés comme des éléments externes
ex : connecteurs BDD

<br>


* 5- Build / Release / Run : ne pas les mélanger (attention dans les pipelines)

--------------------------------------------------------------------------------------

# Suite


<br>


* 6- Processus : sans état (stateless) doit permettre une scalabilité horizontale

<br>


* 7- Association de ports : les services communiquent sur un port

<br>


* 8- Concurrence : scalabilité horizontale = création de processus plutôt CPU/RAM

<br>


* 9- Jetable : arrêt gracieux et relance sans impact = doit savoir où elle en est

<br>


* 10- Parité dev/prod : intégration des développeurs aux déploiements

<br>


* 11- Logs : flux d'évènement (logstash dans ELK...)

<br>


* 12- Processus d'administration : code admin et applicatif ensemble pas de scission
